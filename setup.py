from distutils.core import setup

NAME = 'pybry'

setup(
    name='pybry',
    version='1.0',
    packages=[NAME, ],
    license='MIT License',
    long_description=open('README.md', mode='r').read()
)
